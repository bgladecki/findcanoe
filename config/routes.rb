Rails.application.routes.draw do
  devise_for :users, path_names: {sign_in: "login", sign_out: "logout"} 
  resources :units
  resources :users
  resources :places
  resources :types

  root to: 'search#index'
end