# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email
#User.destroy_all
Place.destroy_all
Type.destroy_all
Unit.destroy_all

Geocoder.configure(:timeout => 20)

all_types = [
  Type.create(name: "Kayak", x_people: 2),
  Type.create(name: "Canoe", x_people: 1),
  Type.create(name: "StandUp Paddle", x_people: 1),
  Type.create(name: "Sea Kayak", x_people: 2)
]

all_places = [
  Place.create(name: "Pensjonat Atmosfera", address: "Leśna 18 97-213 Smardzewice", desc: "Atmosfera to pensjonat położony nad Zalewem Sulejowskim, to idealne miejsce na relaks dla osób ceniących komfort i wysoką jakość obsługi i kameralną atmosferę. To miejsce, gdzie czas płynie wolniej, miejsce gdzie każdy gość traktowany jest indywidualnie. To miejsce, w którym przekonasz się, co naprawdę znaczy polska gościnność."),
  Place.create(name: "Kemping Sielanka Nad Pilicą", address: "Nowy Zjazd lok. 6, 05-660 Warka", desc: "Zaledwie kilkadziesiąt kilometrów na południe od wielkiej aglomeracji warszawskiej rozciąga się malownicza kraina, w której każdy może znaleźć coś ciekawego..."),
  Place.create(name: "\"Kajmar\" Marcin Suszka, Kajaki Sulejów", address: "Sulejów, Częstochowa", desc: "Kilkunastoletnie doświadczenie w spływach kajakowych. Tytuły Instruktorów Turystyki kajakowej i Rekraacji Polskiego Związku Kajakowego III stopnia oraz III miejsce w Otwartych Międzynarodowych Mistrzostwach Polski Instruktorów Kajakarstwa w 2011r oraz przede wszystkim pasja do kajakarstwa pozwala zapewnić naszym klientom najwyższy poziom obsługi."),
  Place.create(name: "Leśna Przystań", address: "Ciemiętniki-Kresy 29-120", desc: "Znajdą nas Państwo w Ciemiętnikach. Jest to urocza wieś leżąca w województwie Świętokrzyskim przy ujściu rzeki Czarnej Włoszczowskiej do Pilicy."),
  Place.create(name: "Miami Vice", address: "Miami", desc: "Best renting place in whole United States. We can provide you with high quality canoes and kayaks."),
  Place.create(name: "Newark Water Palace", address: "Newark NY", desc: "High quality and fast kayaks."),
  Place.create(name: "New York Waterking", address: "New York", desc: "We. are. kings. of. water. Period."),
  Place.create(name: "Norwich Waterflash", address: "Norwich NY", desc: "I dont believe we even have rivers in here. But still, let's make some money on units renting."),
  Place.create(name: "Boston Atlantic Paradise", address: "Boston", desc: "We are crazy about water kayaks. Try us."),
  Place.create(name: "Russian kay", address: "Moscow", desc: "Nu pagadi")
]
all_places.each do |p|
  Random::rand(0..3).times do
    type = all_types[Random::rand(0..3)]
    Unit.create(amount: Random.rand(1..10), place_id: p.id, type_id: type.id)
  end
end

=begin

10.times do 
  place = Place.create(name: Faker::Company.name, latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, desc: Faker::Lorem.sentence)
  Random::rand(0..10).times do 
    place.tag_list.add(Faker::Lorem.word)
  end
  2.times do
    # Type.create(name: Faker::Name.first_name, x_people: Random.rand(1..5))
    # type = Type.create(name: "Regular Classic", x_people: 2)
    type = all_types[Random::rand(0..3)]
	  Unit.create(amount: Random.rand(1..10), place_id: place.id, type_id: type.id)
    #type.Unit.create(amount: Random.rand(1..50))
  end
end
=end
