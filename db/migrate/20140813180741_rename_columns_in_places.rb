class RenameColumnsInPlaces < ActiveRecord::Migration
  def change
    change_table :places do |t|
      t.rename :lat, :latitude
      t.rename :lng, :longitude
    end
  end
end
