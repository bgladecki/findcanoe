class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units do |t|
      t.integer :count
      t.references :place, index: true

      t.timestamps
    end
  end
end
