class CreateTypes < ActiveRecord::Migration
  def change
    create_table :types do |t|
      t.string :name
      t.integer :x_people
      t.string :image
      t.references :unit, index: true

      t.timestamps
    end
  end
end
