class RemoveColumnsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :username    
    remove_column :users, :name
  end
end
