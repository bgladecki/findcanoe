class AddTypeToUnit < ActiveRecord::Migration
  def change
    add_reference :units, :type, index: true
  end
end
