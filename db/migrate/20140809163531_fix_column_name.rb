class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :units, :count, :amount
  end
end
