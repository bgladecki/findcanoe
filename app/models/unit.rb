class Unit < ActiveRecord::Base
  belongs_to :place
  belongs_to :type

  accepts_nested_attributes_for :place
end
