class Place < ActiveRecord::Base 
  
  has_many :units
  has_many :types, through: :units
  accepts_nested_attributes_for :units, allow_destroy: true
  acts_as_taggable
  reverse_geocoded_by :latitude, :longitude
  geocoded_by :address
  after_validation :reverse_geocode  # auto-fetch address
  after_validation :geocode

  def self.search(query)
    where("LOWER(name) LIKE ?", "#{query.downcase}") 
  end

  def self.search_places(search)
    if search
      maps_ids = Place.near(search, 100).map(&:id)
      tags_ids = Place.tagged_with(search, :any => true).map {|p| [p.id] }
      ids = (maps_ids + tags_ids).uniq
      places = Place.find(ids)
    else
      places = Place.all.order('created_at DESC')
    end
    places
  end

end
