json.array!(@types) do |type|
  json.extract! type, :id, :name, :x_people, :image, :unit_id
  json.url type_url(type, format: :json)
end
