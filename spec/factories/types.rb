# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :type do
    name "MyString"
    x_people 1
    image "MyString"
    unit nil
  end
end
