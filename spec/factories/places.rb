# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :place do
    name "MyString"
    lat 1.5
    lng 1.5
    desc "MyText"
  end
end
